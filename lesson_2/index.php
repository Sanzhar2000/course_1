<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        header{
            background: #e3e3e3;
            padding: 2em;
            text-align: center;
        }
    </style>
</head>
<body>
    <header>
        <h1>
            <?=
                "Hello, " . htmlspecialchars($_GET['name']) . " " . htmlspecialchars($_GET['surname']) .
                " whose date of birth is " . htmlspecialchars($_GET['day']) . "." . htmlspecialchars($_GET['month']) . "." . htmlspecialchars($_GET['year']);
            ?>
        </h1>
        // use this link to test http://localhost:8888/?name=Sanzhar&surname=Toktassyn&day=05&month=09&year=2000
    </header>
</body>
</html>
