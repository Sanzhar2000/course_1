<?php

class Post
{
    public $title;

    public $published;

    public function __construct($title, $published)
    {
        $this->title = $title;
        $this->published = $published;
    }
}

$posts = [
    new Post('My First Post', true),
    new Post('My Second Post', true),
    new Post('My Third Post', true),
    new Post('My Fourth Post', false)
];

$modified = array_map(function ($posts) {
    return 'foobar';
    //return $posts->published == false;
}, $posts);

/*$unpublishedPosts = array_filter($posts, function ($posts){
    return $posts->published == false;
});*/
var_dump($modified);