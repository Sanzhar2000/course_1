<?php

class PagesController
{
    public function home()
    {
        //die('home');


        return view('index');
    }

    public function about()
    {
        return view('about');
    }

    public function contact()
    {
        return view('contact');
    }
}
