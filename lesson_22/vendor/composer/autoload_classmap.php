<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'App\\Controllers\\PagesController' => $baseDir . '/App/controllers/PagesController.php',
    'App\\Controllers\\UsersController' => $baseDir . '/App/controllers/UsersController.php',
    'App\\Core\\App' => $baseDir . '/core/App.php',
    'App\\Core\\Request' => $baseDir . '/core/Request.php',
    'App\\Core\\Router' => $baseDir . '/core/Router.php',
    'App\\Models\\Project' => $baseDir . '/App/models/Project.php',
    'ComposerAutoloaderInite18892d70f57b6087b5db180080072a8' => $vendorDir . '/composer/autoload_real.php',
    'Composer\\Autoload\\ClassLoader' => $vendorDir . '/composer/ClassLoader.php',
    'Composer\\Autoload\\ComposerStaticInite18892d70f57b6087b5db180080072a8' => $vendorDir . '/composer/autoload_static.php',
    'Composer\\InstalledVersions' => $vendorDir . '/composer/InstalledVersions.php',
    'Connection' => $baseDir . '/core/database/Connection.php',
    'QueryBuilder' => $baseDir . '/core/database/QueryBuilder.php',
);
